//logging the input argumet.
// n = {1,2,3,4} <-- is labatory nummer
//naam geving/notatie: achternaam_n.pdf

//THIS IS WORKING.
function start (input){
      console.log("input: " + input);

      //The only accepted input.
      var accept = [false, false, true];
      //creating a multi-dimensional-array called next.
      //var next = [[0,1], [0,2], [0,3], [3,3]];
      var next = [[0,1], [0,2],[2,2]]
      console.log("multidemensionalarray: " +  next);
      var state = 0;
      for (var i = 0; i <= (input.length -1); i++)
      {
            console.log("---------------------");
            console.log("teller: " + i);

            //Gets ASCII char code of pointed input and substracted by ASCII code of the character a.
            //The answer will be stored in state, because the input is an 'a' or a 'b' the state (answer) will be either 0 or 1.
            state = next[state][input.charCodeAt(i) - 'a'.charCodeAt()];
            console.log("current letter: " + input[i]);
            console.log("state: " + state);
            console.log("---------------------");
      }

      //This line will print out the final decision.
      console.log("accept: " + accept[state]);
}

console.log("\n");
console.log("IMPORTANT: You can only put arrays of a or b as second argument.");
console.log("©Deno Suresan | Builded with NodeJS")
start(process.argv[2].toString());
