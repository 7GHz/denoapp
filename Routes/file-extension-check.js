var fileExtensionDataToJSON = require("./file-extension-data-toJSON");
var routeHandler = require('./routeHandler');
var routeStylesheet = require('./route-stylesheet')

//This method will find the correct extension when an external file is request
// forexample: .css, , .js, or .img.
function fileExtensionCheck(http){

	var fileJSON = fileExtensionDataToJSON(http);
	//var fileJSON = {extension:"lol"};
	if(fileJSON.extension != "/")
	{
		switch(fileJSON.extension)
		{
			case "ico":
				console.log("The extension is 'ico, there is probably a favicon to load.");
				break;
			case "css":
				console.log("The extension is 'css', there is a stylesheet to load.");
				routeStylesheet(fileJSON, http);
				break;
			case "js":
				console.log("The extension is 'js', there is a javascript page to load.");
				break;
			default:
				console.log("WARNING: Unexpected extension loaded called: " + fileJSON.extension + "");
				break;
		}
	}

	else
	{
		console.log("Normal routing path is requested.");
		console.log("Requested routing path is: " + fileJSON.name)
		console.log("This shouldn't run... check: file-extension-check.js for errors")
		console.log("http log: " + http);
		console.log("END http log.")
		//routeHandler.routeURLs(http);
	}
}
module.exports = fileExtensionCheck;
