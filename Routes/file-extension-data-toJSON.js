
//Converts filepath information into a JSON object..
function fileExtensionDataToJSON(httpparam, callback){
	if(callback != function(){}){
		console.log("Building JSON object for request: ")

		//Gets filepath.
		var filepath = httpparam.pathname.split('/').join('/');

		//Gets file extension of a path.
		var fileExtension = filepath.split('.').pop();

		//Gets filename without extension and path.
		var filename = getFileName(filepath);

		fileData = {
			"name" : filename,
			"extension" : fileExtension,
			"path" : filepath
		};
		console.log(fileData);

		return fileData;
	}
}


//Gets filename without extension and function
function getFileName(filepath){

	//Removes the directory path: "/.../" before test.php
	var fileNameIndex = filepath.lastIndexOf("/") + 1;
	var filename = filepath.substr(fileNameIndex);

	//Search for position of ".", so it slices str[0] till str["."] to a new string.
	var positionOfDot = filename.lastIndexOf('.');
	var filename = filename.slice(0, positionOfDot);
	//console.log("getFileName function creates this: " + filename);

	return filename;
}

module.exports = fileExtensionDataToJSON;
