"use strict"

var fs = require('fs');
var ejs = require('ejs');
var querystring = require('querystring');


var index = function(http){
    fs.readFile('./View/index.ejs', 'UTF-8', function(err,data){
    	if(err)
        {
    		throw err;
    	}
    	else
        {
    		var pageInformation =
            {
    			"Title":"Deno's page",
    			"Slogan":"Mijn website",
    			"Footer":"This website in still in development."
    		}

            http.data += data;
            createPage(http, pageInformation);
        }
    });
}

var userlogin = function(http){
    fs.readFile('./View/userlogin.ejs', 'UTF-8', function(err, data){
        if(err)
        {
            throw err;
        }
        else
        {
            var pageInformation =
            {
                "Title" : "Log here in:",
                "Username":"",
                "Password":"",
            }

            http.data += data;
            createPage(http, pageInformation);
        }
    });
}

var userregister = function(http){
    fs.readFile('./View/userregister.ejs', function(err, data){
        if(err)
        {
            throw err;
        }
        else
        {
            var pageInformation =
            {
                "Username":"",
                "Password":""
            }

            http.data += data;
            createPage(http, pageInformation);
        }
    });
}

var PageNotFound = function(http){
  /*fs.readFile('./View/userregister.ejs', function(err, data){
    if(err){
        throw err;
      }
      else{
          var pageInformation = {
            "SpecificError" : "Sorry, something bad happened, page couldn't be found"
          }
          http.data += data;
          createPage(http, pageInformation);
      }
  });


  fs.readFile(__dirname + "/../Assets/deno-style.css", function (err, data){
    if(err)
    {
      throw err;
    }

    else
    {
      http.response.writeHead(200, {'Content-Type': 'text/css'});
      http.response.write(data);
      http.response.end();
    }
  });*/
}

//Helper methods
var createPage = function (http, pageInformation)
{
    var ejsbody = ejs.render(http.data.toString('binary'),{"pageInformation":pageInformation});
    writeWebsite(ejsbody, http.response)
}
//End Helper methods



//Callback functions
var writeWebsite = function(data, Response)
{
    if(data != null)
    {
        Response.writeHead(200, {"Content-Type":"Text/html"});
        Response.write(data);
        Response.end();
        console.log("Website is written!");
    }
}
//End callback functions

//Public methods.
exports.index = index;
exports.userlogin = userlogin;
exports.userregister = userregister;
exports.PageNotFound = PageNotFound;
//End public methods.
