
//var checkPostReason = require('./check-post-reason');
var inspectObjectToWrite = require('../helper-modules/inspect-object-write-file');
var fileExtensionCheck = require('./file-extension-check');
var routeHandler = require('./routeHandler');

var HTTPMethodRouter = function(httpparam, callback){
	switch(httpparam.request.method){
		case 'POST':
		console.log('HTTP request: POST');
		//checkPostReason(httpparam);
		routeHandler.routeURLs(httpparam);
		break;

		case 'GET':
		console.log('HTTP request: GET');
		/*inspectObjectToWrite("Assets/Log/testlog.txt", httpparam, function(err){
			if(err)
			{
				throw err;
			}

			else
			{
				console.log("File is succesfully written. Name: " + " textlog.txt");
			}
		});*/
		//fileExtensionCheck(httpparam);
		routeHandler.routeURLs(httpparam);
		fileExtensionCheck(httpparam);
		break;

		default:
		console.log('HTTP request: ' + httpparam.request.method);
		console.log('WARNING:' + httpparam.request.method + ' is not supported.')
		routeHandler(httpparam);
		break;
	}
}

module.exports = HTTPMethodRouter;
