var routes = require('./routes');
var loginstatuscheck = require('./loginstatuscheck')

///Routes the user to the correct page.
var routeURLs = function(httpparams){

	// split('/') is being used to prevent the forward dash issues by URL routing. Some
	// browsers like Chrome removes forward slash, while IE keeps it. The solve that problem
	// the split is added.
	//console.log("Without split: " + httpparams.pathname);
	//console.log("With split: " + httpparams.pathname.split('/').join('/'));

	loginstatuscheck(httpparams);

    switch (httpparams.pathname.split('/').join('/')){
        case "/":
            console.log('request: index');
            routes.index(httpparams);
            break;
		case "/login":
			console.log('request: userlogin');
			routes.userlogin(httpparams);
			break;
		case "/register":
			console.log('request: userregister');
			routes.userregister(httpparams);
			break;

		case "/Game/":
			console.log('Game map');
			break;
		default:
			console.log('Different request: ' + httpparams.pathname.split('/').join('/'));
			routes.PageNotFound(httpparams);
			//routes.notfound();
			break;
	}
}// ; of niet?

exports.routeURLs = routeURLs;
