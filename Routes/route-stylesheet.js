var fs = require('fs');
var querystring = require('querystring');
var stylesheetHandler = require('./stylesheet-handler');

//This will find the correct CSS stylesheet.
function routeStylesheet(fileJSON, httpparam)
{
  switch(fileJSON.name)
  {
    case "deno-style":
      stylesheetHandler.denoStyle(httpparam);
      console.log("loading deno-style.css stylesheet");
      break;
    default:
      console.log("no stylesheet detected and no action taken. But where does this stylesheet: " + fileJSON.name + " come from?");
      break;
  }
}

module.exports = routeStylesheet;
