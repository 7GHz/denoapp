var fs = require('fs');
var ejs = require('ejs');
var querystring = require('querystring');

function denoStyle(httpparam)
{
  fs.readFile(__dirname + "/../Assets/deno-style.css", function (err, data){
    if(err)
    {
      throw err;
    }

    else
    {
      activateStylesheet(data, httpparam);
    }
  });
}

function activateStylesheet(data, httpparam)
{
  httpparam.response.writeHead(200, {'Content-Type': 'text/css'});
  httpparam.response.write(data);
  httpparam.response.end();
}

exports.denoStyle = denoStyle;
