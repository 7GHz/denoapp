var mongodb = require('mongodb');
var mongoClient = mongodb.MongoClient;
var assert = require('assert');

var url = 'mongodb://localhost:27017/denoscratchdb';
var mongoDBConnect = mongodb.connect(url, function(err, db){
	assert.equal(null, err);
	console.log('Connected correctly to server');



	//closing db is probably not needed
	//db.close();
});

module.exports = mongoDBConnect;