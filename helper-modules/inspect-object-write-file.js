var util = require('util');
var fs = require('fs');

function InspectObjectAndWrite(filename, objectdata, callback){
	fs.writeFile(filename, util.inspect(objectdata, {"showhidden":false, "depth":null}), callback);
}

module.exports = InspectObjectAndWrite;