var http = require('http');
var port = process.env.port || 1337;
var url = require('url');
var routeHandler = require('./Routes/routeHandler');
var HTTPMethodRouter = require('./Routes/HTTPMethodRouter');

var util = require('util');

http.createServer(function (req, res) {
    var pathname = url.parse(req.url, true).pathname;


    var httpparam = {
    	"request":req,
    	"response":res,
    	"pathname":pathname
    };
    //console.log(body);
    //console.log(necessary.pathname);

    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
    + (currentdate.getMonth()+1)  + "/"
    + currentdate.getFullYear() + " @ "
    + currentdate.getHours() + ":"
    + currentdate.getMinutes() + ":"
    + currentdate.getSeconds();

    console.log(" ");
    console.log("--------------------------------");
    console.log("Request time&date: " + datetime);
    console.log("IP Adress of the request: " + req.connection.remoteAddress); // req.connection.remoteAddress get's the ip address of the user.
    console.log("--------------------------------");
    console.log(" ");
    HTTPMethodRouter(httpparam);
	//routeHandler.routeURLs(httpparam);
  console.log(" ");
    }).listen(port);
console.log("Listening on port: " + port);
