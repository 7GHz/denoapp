module.exports = function(){
	return function(context){
	
		if(context.session.loggedin)
		{
			//show logout button
			context.output.write('<a href="/logout" class="button">');
			context.output.write('Log out');
			context.output.write('</a>');
		}

		else
		{
			//show login button
			context.output.write('<a href="/login" class="button">');
			context.output.write('Log in');
			context.output.write('</a>');
			
			//show register button
			context.output.write('<a href="/register" class="button">');
			context.output.write('Registreer');
			context.output.write('</a>');
		}

		context.finish();
	}
}