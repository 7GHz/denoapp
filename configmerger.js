var fs = require('fs');
var lodash = require('lodash');

//Merge 2 object into 1.
//Output filename = the input file 
function compile(input_filename, output_filename, callback)
{
	var output_object = {};
	var input_object = require(input_filename);

	try
	{
		require.resolve(output_filename);
		output_object = require(output_filename);	
	}
	catch(error)
	{
	}

	var merged_object = lodash.merge(output_object, input_object);
	
	fs.writeFile(output_filename, JSON.stringify(merged_object, null, 2), function(error) {

		if(error)
		{
			console.error('json-merge: could not write output file: message="' + error.message + '"');
			exit(1);
		}
		else
		{
			callback();
		}

	});
}

compile(process.argv[2], process.argv[3], function(result){
	//finished
})